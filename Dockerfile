FROM node:alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app
RUN npm install && npm cache clean

# Build client
COPY public /usr/src/app/public
COPY src /usr/src/app/src
RUN npm run build:client

# Build server
COPY server /usr/src/app/server
RUN npm run build:server

# Remove intermediate stuff not needed to deploy server
RUN npm prune --production
RUN rm -r ./src ./public ./server

# Start server
CMD [ "npm", "run", "server:prod"]
