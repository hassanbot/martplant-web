# Martplant-web

## Prerequisites
- Download and install `nodejs`: https://nodejs.org/en/download/
- Download and install `mongodb`: https://docs.mongodb.com/manual/installation/

## Installing
From the main directory, run `npm install` to install the dependencies.

## Running
Start MongoDB from the main directory:

```
path/to/mongo/executable --dbpath data
```

Start the server on `localhost:3001`:

```
npm run server:dev
```

Start the webpage on `localhost:3000`:
```
npm run start-dev
```

On Windows, the npm calls will have to be appended with `:win`:
```
npm run server:dev:win
npm run start-dev:win
```

## Using the server API
To post a measurement, use the following REST API call:
```
localhost:3001/api/measurement/new/[value]
```

Enjoy!