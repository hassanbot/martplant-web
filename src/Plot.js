// Components
import React from 'react'
import { ChartContainer, ChartRow, Charts, AreaChart, Resizable, YAxis } from "react-timeseries-charts";
import { observer } from 'mobx-react';

// Internal imports
import './styles/Plot.css'

const Plot = observer(class Plot extends React.Component {
    handleZoom = timerange => {
        this.props.store.setTimerange(timerange);
    };

    render() {
        const { store } = this.props;
        return (
            <div className={'Plot'}>
                <Resizable>
                    <ChartContainer timeRange={store.timerange}
                                    // width={400}
                                    enablePanZoom={true}
                                    onTimeRangeChanged={this.handleZoom}
                                    showGrid={true}>
                        <ChartRow height="200">
                            <YAxis id="axis1"
                                   label="MOISTURE"
                                   min={ store.smoothedTimeSeries.min('value')}
                                   max={ store.smoothedTimeSeries.max('value')}
                                   type="linear"
                                   format="5d"
                            />
                            <Charts>
                                <AreaChart axis="axis1" series={store.smoothedTimeSeries}/>
                            </Charts>
                        </ChartRow>
                    </ChartContainer>
                </Resizable>
            </div>
        );
    }
});

export default Plot;
