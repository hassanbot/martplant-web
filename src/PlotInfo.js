import React from 'react';
import { ButtonGroup, Button, Grid, Row, Col } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';
import { observer } from 'mobx-react';

const PlotInfo = observer(class PlotInfo extends React.Component {
    postData = () => {
        let formData = new FormData();
        formData.append("json", JSON.stringify({value: 44}));
        fetch("http://217.72.59.25:3001/api/measurements", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({value: 123})
        })
        .then(res => {
            this.props.store.downloadData();
        })
    };

    render() {
        const { store } = this.props;
        const refreshSymbol = (store.isRefreshing.get())
            ? <FontAwesome name="refresh" spin size="2x"/>
            : <FontAwesome name="check" size="2x"/>;

        return (
            <Grid>
                <Row className="show-grid">
                    <div style={{padding: '10px'}}/>
                    <Col xs={11} md={3}>
                        <ButtonGroup block vertical>
                            <Button block onClick={store.downloadData}>
                                Refresh
                            </Button>
                        </ButtonGroup>
                    </Col>
                    <Col xs={1} md={1}>
                        {refreshSymbol}
                    </Col>
                </Row>
            </Grid>
        );
    }
});

export default PlotInfo;
