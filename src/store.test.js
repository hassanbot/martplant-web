import { smoothPoints, removeOutliers } from './store';

it('smoothing applied correctly', () => {
    let values = [2, 4, 6, 8, 10, 12, 14, 16];
    let dates = [0, 1, 2, 3, 4, 5, 6, 7];
    let points = values.map((v, i) => [dates[i], v]);

    let smoothing_0 = smoothPoints(points, 0);
    expect(smoothing_0).toEqual(points);

    let smoothing_1 = smoothPoints(points, 1);
    expect(smoothing_1).toEqual(points.slice(1, -1));
});


it('outliers removed correctly', () => {
    let values = [2, 4, 6, 8, 10, 12, 14, 16];
    let dates = [0, 1, 2, 3, 4, 5, 6, 7];
    let points = values.map((v, i) => [dates[i], v]);

    let outliers_0 = removeOutliers(values.slice(), 0);
    expect(outliers_0).toEqual(values);

    let outliers_1 = removeOutliers(values.slice(), 1);
    expect(outliers_1).toEqual(values.slice(1, -1))
});

it('dates rendered correctly when removing outliers', () => {
    let values = [2, 4, 6, 8, 10, 12, 14, 16];
    let dates = [0, 1, 2, 3, 4, 5, 6, 7];
    let points = values.map((v, i) => [dates[i], v]);

    let points_0_1 = smoothPoints(points.slice(), 0, 1);
    let dates_0_1 = points_0_1.map(e => e[0]);
    expect(dates_0_1).toEqual(dates.slice(1, -1));
});
