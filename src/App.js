// Components
import React, { Component } from 'react';

// Internal imports
import './styles/App.css';
import Header from './Header';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import PlotContainer from "./PlotContainer";


class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <PlotContainer header="martin" store={this.props.store}/>
            </div>
        );
    }
}

export default App;
