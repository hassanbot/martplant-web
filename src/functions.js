// Utility functions

export function validateNumber(number) {
    if (typeof number !== 'number' || number < 0) return 'error';
    else if (number > 2000) return 'warning';
    else return 'success';
}
