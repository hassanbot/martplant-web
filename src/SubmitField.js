import React from 'react'
import { Button, ControlLabel, FormControl, FormGroup, InputGroup } from "react-bootstrap";

class SubmitField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
    }

    handleSubmit = e => {
        this.setState(e.target.value);
        this.props.handleSubmit(value);
    };

    render() {
        return (
            <FormGroup>
                <ControlLabel>this.props.label</ControlLabel>
                {' '}
                <InputGroup>
                    <InputGroup.Button>
                        <Button onClick={this.props.handleClick} type="submit">this.props.buttonText</Button>
                    </InputGroup.Button>
                    <FormControl type="number"
                                 value={store.smoothing}
                                 placeholder="Input number"
                                 onChange={this.handleSmoothing}
                                 ref={(input) => { this.textInput = input; }}
                    />
                </InputGroup>

            </FormGroup>
        );
    }
}

export default SubmitField;
