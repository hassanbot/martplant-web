import React from 'react'
import 'rc-collapse/assets/index.css';
import Collapse, { Panel } from 'rc-collapse';
import Plot from './Plot'
import './styles/PlotContainer.css'
import PlotControl from "./PlotControl";
import { validateNumber } from './functions';
import PlotInfo from "./PlotInfo";


class PlotContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
    }

    handleChange = (e) => {
        let value = e.target.value;
        // eslint-disable-next-line
        if (parseInt(value, 10)==value)
            value = parseInt(value, 10);
        this.setState({value: value})
    };

    handleSubmit = () => {
        if (validateNumber(this.state.value) === 'error')
            return;
        this.props.store.submitValue(this.valueInput.props.value);
    };

    render() {
        return (
            <div>
                <Collapse accordion={true}>
                    <Panel header={this.props.header}>
                        <Plot store={this.props.store}/>
                        <PlotControl store={this.props.store}/>
                        <PlotInfo store={this.props.store}/>
                    </Panel>
                </Collapse>
            </div>
        );
    }
}

PlotContainer.propTypes = {
    header: React.PropTypes.string,
    store: React.PropTypes.any
};

export default PlotContainer;
