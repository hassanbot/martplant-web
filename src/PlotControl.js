import React from 'react'
import { Button, ControlLabel, FormControl, FormGroup, InputGroup } from "react-bootstrap";

class PlotControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mean: '',
            median: ''
        };
    }

    handleSmoothing = e => {
        this.setState({mean: e.target.value});
    };

    handleSubmitSmoothing = e => {
        this.props.store.smoothing.set(parseInt(this.smoothingInput.props.value, 10));
    };

    handleOutliers = e => {
        this.props.store.outliers.set(parseInt(e.target.value, 10));
    };

    handleMedian = e => {
        this.setState({median: e.target.value});
    };

    handleSubmitMedian = e => {
        this.props.store.median.set(parseInt(this.medianInput.props.value, 10));
    };

    render() {
        return (
            <FormGroup bsClass="form-inline">
                <FormGroup>
                    <ControlLabel>Smoothing</ControlLabel>
                    {' '}
                    <InputGroup>
                        <InputGroup.Button>
                            <Button onClick={this.handleSubmitSmoothing} type="submit">Submit value</Button>
                        </InputGroup.Button>
                        <FormControl type="number"
                                     value={this.state.mean}
                                     placeholder="Input number"
                                     onChange={this.handleSmoothing}
                                     ref={(input) => { this.smoothingInput = input; }}
                        />
                    </InputGroup>

                </FormGroup>
                {' '}
                <FormGroup>
                    <ControlLabel>Median</ControlLabel>
                    {' '}
                    <InputGroup>
                        <InputGroup.Button>
                            <Button onClick={this.handleSubmitMedian} type="submit">Submit value</Button>
                        </InputGroup.Button>
                        <FormControl type="number"
                                     value={this.state.median}
                                     placeholder="Input number"
                                     onChange={this.handleMedian}
                                     ref={(input) => { this.medianInput = input; }}
                        />
                    </InputGroup>
                </FormGroup>

            </FormGroup>
        );
    }
}

export default PlotControl;
