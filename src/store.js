import { extendObservable, observable, action } from 'mobx';
import { TimeSeries, TimeRange } from 'pondjs';

let prefix = '';
if (process.env.NODE_ENV === 'development')
    prefix = 'http://localhost:3001';

export function applyMedianFilter(arr, median_length) {
    if (arr.length < median_length*2)
        return -1;

    let filteredArr = [];
    for (let i = 0; i < arr.length - 2*median_length; ++i) {
        const sortedArr = arr.slice(i, i + median_length*2 + 1).sort((a, b) => a - b);
        const medianValue = sortedArr[(sortedArr.length - 1)/2];
        filteredArr.push(medianValue);
    }
    return filteredArr;
}

export function applyMeanFilter(arr, mean_length) {
    if (arr.length < mean_length*2)
        return arr;

    let filteredArr = [];
    for (let i = 0; i < arr.length - 2*mean_length; ++i) {
        let summedValue = arr.slice(i, i + mean_length*2 + 1).reduce((a, b) => a + b, 0);
        filteredArr.push(summedValue/(2*mean_length+1));
    }
    return filteredArr;
}

export function assembleTimeSeries(dates, values) {
    if (values.length !== dates.length) {
        console.error("Values and dates arrays are of different lengths");
        return -1
    }

    let points = [];
    for (let i = 0; i < values.length; ++i) {
        points.push([dates[i], values[i]]);
    }
    return new TimeSeries({
        'name': 'measurements',
        'columns': ['time', 'value'],
        'points': points
    });
}

class Store {
    // Points, where each point is an ms timestamp and a value
    points = observable([[Date.now().valueOf(), 0]]);

    // Internal timerange as a list of ints (ms timestamps)
    _timerange = observable([0, 0]);

    // Are we refreshing?
    isRefreshing = observable(false);

    // How much smoothing?
    smoothing = observable(0);

    // How many values to take median from?
    median = observable(0);

    constructor() {
        extendObservable(this, {
            get smoothedTimeSeries() {
                // Apply median filter
                let filteredValues = applyMedianFilter(this.points.map(e => e[1]), this.median);
                let dateValues = this.points.slice(this.median, this.points.length - this.median).map(e => e[0]);

                // Apply mean filter
                filteredValues = applyMeanFilter(filteredValues, this.smoothing);
                dateValues = applyMeanFilter(dateValues, this.smoothing);

                return assembleTimeSeries(dateValues, filteredValues);
            },

            get timerange() {
                return (this._timerange[1] - this._timerange[0] === 0)
                    ? this.smoothedTimeSeries.timerange() : new TimeRange(this._timerange.slice());
            },

            setTimerange: action(tr => {
                // Have to convert the TimeRange to a list of ints to be able to Observe it
                this._timerange.replace([
                    tr.range().first().valueOf(),
                    tr.range().last().valueOf()
                ]);
            }),

            downloadData: action(() => {
                this.isRefreshing.set(true);
                fetch(prefix + "/api/measurements/0", {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json'}
                })
                .then(res => {
                    return res.json();
                })
                .then(jsonRes => {
                    this.points.replace(jsonRes.map(element => [new Date(element.date).valueOf(), element.value]));
                    this.isRefreshing.set(false);
                });
            }),

            submitValue: action(value => {
                this.points.push([Date.now().valueOf(), value]);
                fetch(prefix + "/api/measurement/new/" + value, {
                    method: 'GET',
                    mode: 'cors'
                })
                .then(res => {
                    this.downloadData();
                    return res.text();
                })
                .then(res => {
                    console.log(res);
                });
            })

        });

        this.downloadData();
    }
}

export default new Store();
