// Components
import React from 'react';
import { PageHeader } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';

// Internal imports
import './styles/Header.css';

class Header extends React.Component {
    //
    render() {
        return (
            <PageHeader>
                <FontAwesome name="snowflake-o" className="App-logo" size="2x"/>
                <div>
                    Välkommen till MARTINS HEMSIDA
                </div>
            </PageHeader>
        );
    }
}

export default Header;
