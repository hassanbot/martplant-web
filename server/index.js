import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import './mongo';
import { getMeasurements, newMeasurement } from './measurement';

// ----- SETUP APP -----
const app = express();

// Serve static build directory if in production
if (process.env.NODE_ENV === 'production')
    app.use(express.static('build'));

app.use(bodyParser.urlencoded({ extended: true })); // Needed for POST
app.use(bodyParser.json()); // Setup body parser

app.use(morgan('short'));

// Handle access control (CORS)
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');

    // // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Origin, Accept');

    // // Set to true if you need the website to include cookies in the requests sent
    // // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


// ----- SETUP ROUTES -----
// The functions are defined in ./mongo.js
const router = express.Router();

// Get all measurements
router.get('/measurements', getMeasurements);

// Get all measurements
router.get('/measurements/:sensor', (req, res, next) => {
    getMeasurements(req, res, next, req.params.sensor)
});

// Create new measurement by GET
router.get('/measurement/new/:value', (req, res) => {
    res.send(newMeasurement(req.params.value));
});

// Create new measurement by GET
router.get('/measurement/new/:sensor/:value', (req, res) => {
    res.send(newMeasurement(req.params.value, req.params.sensor));
});

// Create new measurement by POST
router.post('/measurements', (req, res) => {
    res.type('text/plain');
    res.send(newMeasurement(req.body.value));
});

// The api will be prefixed with /api
app.use('/api', router);


// ----- START SERVING -----
// Listen on port specified in environment or default to 3001
const port = parseInt(process.env.MARTPLANT_SERVER_PORT) || 3001;
app.listen(port, () => {
    console.log('Example app listening on port ' + port);
});
