import mongoose from 'mongoose';

// Create a schema with a value and a date entry
let measurementSchema = new mongoose.Schema({
    value: {
        type: Number,
        min: 0,
        required: true
    },
    date: {
        type: Date,
        default: Date.now,
    }
});

let sensors = [];
sensors.push(mongoose.model('measurement', measurementSchema));
sensors.push(mongoose.model('sensor1', measurementSchema, 'sensor1'));
sensors.push(mongoose.model('sensor2', measurementSchema, 'sensor2'));

export const newMeasurement = (value, sensor=0) => {
	let Measurement = sensors[sensor];
    let measurement = new Measurement();
    // let measurement = new mongoose.model('measurement');
    measurement.value = value;

    measurement.save((err) => {
        if (err) return console.error(err);
    });
    return "Measurement posted: " + value;
};

export const getMeasurements = function(req, res, next, sensor=0) {
	let Measurement = sensors[sensor];
    Measurement.find(function(err, measurements) {
        if (err) return console.error(err);
        const maxVal = 2000;  // TODO: This shouldn't be hardcoded
        let ret_arr = [];
        if (measurements.length > maxVal) {
            const delta = Math.floor(measurements.length / maxVal);
            for (let i = 0; i < measurements.length; i = i + delta) {
                ret_arr.push(measurements[i]);
            }
        } else {
            ret_arr = measurements;
        }
        res.json(ret_arr);
    });
};
