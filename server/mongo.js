// Import and connect to Mongo database
import mongoose from 'mongoose';

const db_adress = (process.env.NODE_ENV === 'production') ? 'database' : 'localhost';
const db_name = process.env.DB_NAME || 'martplant';
mongoose.connect('mongodb://' + db_adress + '/' + db_name);

// Check that the connection is good
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log("Mongoose connected!");
});

